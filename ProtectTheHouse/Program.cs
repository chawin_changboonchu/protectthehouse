﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProtectTheHouse
{
    class Program
    {
        private static Dictionary<int, long> Levels = new Dictionary<int, long>();
        static void Main(string[] args)
        {
            Levels.Add(1, 1);
            Levels.Add(2, 3);
            Levels.Add(3, 5);
            Levels.Add(4, 7);
            Levels.Add(5, 9);
            Levels.Add(6, 11);
            Levels.Add(7, 15);
            Levels.Add(8, 19);
            Levels.Add(9, 25);
            Levels.Add(10, 32);
            Levels.Add(11, 41);
            Levels.Add(12, 54);
            Levels.Add(13, 69);
            Levels.Add(14, 90);
            Levels.Add(15, 117);
            Levels.Add(16, 152);
            Levels.Add(17, 197);
            Levels.Add(18, 256);
            Levels.Add(19, 333);
            Levels.Add(20, 433);
            Levels.Add(21, 563);
            Levels.Add(22, 731);
            Levels.Add(23, 951);
            Levels.Add(24, 1236);
            Levels.Add(25, 1606);
            Levels.Add(26, 2088);
            Levels.Add(27, 2715);
            Levels.Add(28, 3529);
            Levels.Add(29, 4587);
            Levels.Add(30, 5963);
            Levels.Add(31, 7752);
            Levels.Add(32, 10077);
            Levels.Add(33, 13100);
            Levels.Add(34, 17030);
            Levels.Add(35, 22139);
            Levels.Add(36, 28781);
            Levels.Add(37, 37415);
            Levels.Add(38, 48640);
            Levels.Add(39, 63232);
            Levels.Add(40, 82201);
            Levels.Add(41, 106861);
            Levels.Add(42, 138919);
            Levels.Add(43, 180595);
            Levels.Add(44, 234773);
            Levels.Add(45, 305205);
            Levels.Add(46, 396766);
            Levels.Add(47, 515796);
            Levels.Add(48, 670535);
            Levels.Add(49, 871695);
            Levels.Add(50, 1133203);
            Levels.Add(51, 1473164);
            Levels.Add(52, 1915113);
            Levels.Add(53, 2489647);
            Levels.Add(54, 3236540);
            Levels.Add(55, 4207502);
            Levels.Add(56, 5469753);
            Levels.Add(57, 7110679);
            Levels.Add(58, 9243882);
            Levels.Add(59, 12017047);
            Levels.Add(60, 15622161);
            Levels.Add(61, 20308809);
            Levels.Add(62, 26401451);
            Levels.Add(63, 34321886);
            Levels.Add(64, 44618452);
            Levels.Add(65, 58003988);
            Levels.Add(66, 75405184);
            Levels.Add(67, 98026739);
            Levels.Add(68, 127434760);
            Levels.Add(69, 165665188);
            Levels.Add(70, 215364744);
            Levels.Add(71, 279974168);
            Levels.Add(72, 363966418);
            Levels.Add(73, 473156343);
            Levels.Add(74, 615103245);
            Levels.Add(75, 799634219);
            Levels.Add(76, 1039524484);
            Levels.Add(77, 1351381830);
            Levels.Add(78, 1756796378);
            Levels.Add(79, 2283835292);
            Levels.Add(80, 2968985879);
            Levels.Add(81, 3859681643);
            Levels.Add(82, 5017586135);
            Levels.Add(83, 6522861976);
            Levels.Add(84, 8479720568);
            Levels.Add(85, 11023636739);
            Levels.Add(86, 14330727760);
            Levels.Add(87, 18629946088);
            Levels.Add(88, 24218929914);
            Levels.Add(89, 31484608888);
            Levels.Add(90, 40929991555);
            Levels.Add(91, 53208989021);
            Levels.Add(92, 69171685727);
            Levels.Add(93, 89923191445);

            double houseHp = 2000;

            Weapon sword = new Weapon
            {
                name = "Excaliber",
                maxAmmo = 0,
                ammo = 0,
                maxAtkPower = 256,
                minAtkPower = 512,
                maxRange = 1,
                minRange = 0,
                atkSpeed = 4,
                targetNumber = 1,
                accuracy = 0.99
            };

            Weapon handgun = new Weapon
            {
                name = "Handgun",
                maxAmmo = 25,
                ammo = 25,
                maxAtkPower = 32,
                minAtkPower = 27,
                maxRange = 500,
                minRange = 100,
                atkSpeed = 9,
                targetNumber = 1,
                accuracy = 0.99
            };

            Weapon rifle = new Weapon
            {
                name = "Rifle",
                maxAmmo = 7,
                ammo = 7,
                maxAtkPower = 75,
                minAtkPower = 60,
                maxRange = 1500,
                minRange = 500,
                atkSpeed = 14,
                targetNumber = 1,
                accuracy = 0.66
            };

            Weapon machineGun = new Weapon
            {
                name = "Machine gun",
                maxAmmo = 50,
                ammo = 50,
                maxAtkPower = 25,
                minAtkPower = 5,
                maxRange = 800,
                minRange = 200,
                atkSpeed = 5,
                targetNumber = 1,
                accuracy = 0.40
            };

            Weapon shotgun = new Weapon
            {
                name = "Shotgun",
                maxAmmo = 5,
                ammo = 5,
                maxAtkPower = 500,
                minAtkPower = 200,
                maxRange = 400,
                minRange = 1,
                atkSpeed = 69,
                targetNumber = 3,
                accuracy = 0.85
            };
            Character john = new Character()
            {
                name = "John",
                mainWeapon = rifle,
                meleeWeapon = sword,
                killCount = 0,
                level = 1,
                exp = 0,
                accuracy = 1,
                power = 1,
                speed = 1
            };

            Character ben = new Character()
            {
                name = "Ben",
                mainWeapon = handgun,
                meleeWeapon = sword,
                killCount = 0,
                level = 1,
                exp = 0,
                accuracy = 1,
                power = 1,
                speed = 1
            };

            Character chris = new Character()
            {
                name = "Chris",
                mainWeapon = shotgun,
                meleeWeapon = sword,
                killCount = 0,
                level = 1,
                exp = 0,
                accuracy = 1,
                power = 1,
                speed = 1
            };

            Character sham = new Character()
            {
                name = "Sham",
                mainWeapon = machineGun,
                meleeWeapon = sword,
                killCount = 0,
                level = 1,
                exp = 0,
                accuracy = 1,
                power = 1,
                speed = 1
            };

            Enemy zombie = new Enemy()
            {
                name = "zombie1",
                distance = 1500,
                hp = 1000,
                expPoint = 2,
                maxAtkPower = 20,
                minAtkPower = 1
            };

            int countZombie = 2;

            List<Character> listChar = new List<Character>();
            List<Enemy> listEnemy = new List<Enemy>();

            listChar.Add(john);
            listChar.Add(ben);
            listChar.Add(chris);
            listChar.Add(sham);

            listEnemy.Add(zombie);



            long prevTick = 0;
            Random random = new Random();
            var lost = false;

            for (long i = 0; i < 999999999999999999; i++)
            {
                //if (listEnemy.Count == 0)
                //{
                //    Console.WriteLine("You are victorious!");
                //    break;
                //}
                var tick = DateTime.Now.Ticks / 1000000;
                if (random.Next(0, 70000000) == 50)
                {
                    var zombieHp = random.Next(100, 1000);
                    listEnemy.Add(new Enemy
                    {
                        name = "zombie" + countZombie++,
                        distance = 1500,
                        hp = zombieHp,
                        expPoint = Convert.ToInt32(Math.Round(Convert.ToDouble(zombieHp / 100), 0)),
                        maxAtkPower = 20,
                        minAtkPower = 1
                    });
                    Console.WriteLine("New zombie appears!!!");
                }
                //Console.WriteLine(tick % john.speed);
                if (tick != prevTick)
                {

                    //Console.WriteLine("Zombie walks closer and now at distance " + zombie.distance--);
                    foreach (var e in listEnemy)
                    {
                        e.distance--;
                    }
                    foreach (var c in listChar)
                    {
                        if (listEnemy.Any(e => e.distance < 1))
                        {
                            var target = listEnemy.FirstOrDefault(e => e.distance <= 0);
                            if (target != null)
                            {
                                var atkPower = random.NextDouble() * (c.meleeWeapon.maxAtkPower - c.meleeWeapon.minAtkPower) + c.meleeWeapon.minAtkPower + c.power / 3;
                                var percentHit = ((100.00 - ((100.00 / ((double)c.meleeWeapon.maxRange - (double)c.meleeWeapon.minRange)) * ((target.distance - c.meleeWeapon.minRange) / 1.5))) / 100) * (c.meleeWeapon.accuracy + (c.accuracy / 10));
                                if (random.NextDouble() <= percentHit)
                                {
                                    target.hp = target.hp - atkPower < 0 ? 0 : target.hp - atkPower;
                                    Console.WriteLine(c.name + " attacks " + target.name + " with melee weapon for " + atkPower.ToString("N2") + " and " + target.name + " hp is now " + target.hp.ToString("N2"));
                                    if (target.hp <= 0)
                                    {
                                        Console.WriteLine(target.name + " is killed by " + c.name);
                                        listEnemy.Remove(target);
                                        c.killCount++;
                                        c.exp += target.expPoint;
                                        var targetLevel = Levels.OrderByDescending(l => l.Value).FirstOrDefault(l => l.Value <= c.exp);
                                        while (targetLevel.Key > c.level)
                                        {
                                            c.level++;
                                            c.power++;
                                            c.speed++;
                                            c.accuracy++;
                                            Console.WriteLine(c.name + " levels up to level " + c.level + " now " + c.name + " has power " + c.power + ", speed " + c.speed + ", accuracy " + c.accuracy);
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine(c.name + " attacks " + target.name + ", but missed.");
                                }
                            }
                        }
                        else if (tick % (c.mainWeapon.atkSpeed - Math.Round((double)((c.mainWeapon.atkSpeed * (c.speed*0.15)))/100,0)) == 0)
                        {
                            var targets = listEnemy.Where(e => e.distance >= c.mainWeapon.minRange && e.distance <= c.mainWeapon.maxRange).OrderBy(e => e.distance).Take(c.mainWeapon.targetNumber);

                            if (c.mainWeapon.ammo <= 0)
                            {
                                Console.WriteLine(c.name + " is reloading.");
                                c.mainWeapon.ammo = c.mainWeapon.maxAmmo;
                            }
                            else
                            {
                                if (targets != null && targets.Count() > 0)
                                {
                                    var atkPower = random.NextDouble() * (c.mainWeapon.maxAtkPower - c.mainWeapon.minAtkPower) + c.mainWeapon.minAtkPower + c.power / 3;
                                    //var atkPower = random.Next(c.mainWeapon.minAtkPower, c.mainWeapon.maxAtkPower + 1);
                                    foreach (var target in targets)
                                    {
                                        var percentHit = ((100.00 - ((100.00 / ((double)c.mainWeapon.maxRange - (double)c.mainWeapon.minRange)) * ((target.distance - c.mainWeapon.minRange) / 1.5))) / 100) * (c.mainWeapon.accuracy + (c.accuracy / 10));
                                        if (random.NextDouble() <= percentHit)
                                        {
                                            target.hp = target.hp - atkPower < 0 ? 0 : target.hp - atkPower;
                                            Console.WriteLine(c.name + " fires at " + target.name + " at distance " + target.distance + " for " + atkPower.ToString("N2") + " and " + target.name + " hp is now " + target.hp.ToString("N2"));
                                            if (target.hp <= 0)
                                            {
                                                Console.WriteLine(target.name + " is killed by " + c.name);
                                                listEnemy.Remove(target);
                                                c.killCount++;
                                                c.exp += target.expPoint;
                                                var targetLevel = Levels.OrderByDescending(l => l.Value).FirstOrDefault(l => l.Value <= c.exp);
                                                while (targetLevel.Key > c.level)
                                                {
                                                    c.level++;
                                                    c.power++;
                                                    c.speed++;
                                                    c.accuracy++;
                                                    Console.WriteLine(c.name + " levels up to level " + c.level + " now " + c.name + " has power " + c.power + ", speed " + c.speed + ", accuracy " + c.accuracy);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine(c.name + " fires at " + target.name + "!!, but missed " + target.name + " is at distance " + target.distance);
                                        }
                                    }
                                    c.mainWeapon.ammo--;
                                }
                                else if (c.mainWeapon.ammo != c.mainWeapon.maxAmmo)
                                {
                                    Console.WriteLine(c.name + " is reloading.");
                                    c.mainWeapon.ammo = c.mainWeapon.maxAmmo;
                                }
                            }
                        }
                    }

                    foreach (var e in listEnemy)
                    {
                        if (e.distance < 1)
                        {
                            var atkPower = random.NextDouble() * (e.maxAtkPower - e.minAtkPower) + e.minAtkPower;
                            houseHp -= atkPower;
                            Console.WriteLine(e.name + " attacks the house! THe house HP is now at " + houseHp);
                        }
                    }

                    //if (listEnemy.Count > 0)
                    //{
                    //    listEnemy.Remove(listEnemy.FirstOrDefault(e => e.hp == 0));
                    //}
                }

                prevTick = tick;
                if (houseHp <= 0)
                {
                    Console.WriteLine("House is destroyed!! Everyone is killed!! You lost.");
                    lost = true;
                    break;
                }

            }
            Console.WriteLine(long.MaxValue);
            foreach (var character in listChar)
            {
                Console.WriteLine(character.name + " has killed " + character.killCount + " zombies");
            }
            Console.WriteLine("End!");
            Console.ReadLine();
        }
    }

    public class Character
    {
        public string name { get; set; }
        public Weapon mainWeapon { get; set; }
        public Weapon meleeWeapon { get; set; }
        public int killCount { get; set; }

        public int exp { get; set; }
        public int level { get; set; }
        public int accuracy { get; set; }
        public int speed { get; set; }
        public int power { get; set; }
    }

    public class Enemy
    {
        public string name { get; set; }
        public int distance { get; set; }
        public double hp { get; set; }
        public int expPoint { get; set; }
        public double maxAtkPower { get; set; }
        public double minAtkPower { get; set; }
    }

    public class Weapon
    {
        public string name { get; set; }
        public double maxAtkPower { get; set; }
        public double minAtkPower { get; set; }
        public int maxRange { get; set; }
        public int minRange { get; set; }
        public int maxAmmo { get; set; }
        public int ammo { get; set; }
        public int atkSpeed { get; set; }
        public int targetNumber { get; set; }
        public double accuracy { get; set; }
    }
}
